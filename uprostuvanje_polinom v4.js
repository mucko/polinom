// Упростување на алгебарски изрази до полиномен облик

//
'use strict';


class Polinom {

  // конструктор за нов полином како објект
  constructor(dadenP) {
    // како вредност на dadenP може да биде: низа од коефициенти,
    // константа (број) или променлива x

    if (Array.isArray(dadenP)) {

      // правиме копија на влезната низа од коефициенти
      // кои се од облик a_0, a_1, ... , a_n
      this.a = dadenP.slice();

      //ги бришеме нулите и непостоечките коефициенти оддесно
      this.srediNuli();

      console.log(this.a);

    }
    else if (dadenP == "x")
    // единствена дозволена променлива е x
      this.a = [0, 1];

    else if (!isNaN(dadenP))
    // последна легитивмна можност е dadenP да е број
      this.a = [parseFloat(dadenP)];

    else
      console.log("Greshka: od vlezniot podatok " + dadenP + " ne mozhe da se napravi polinom.");

  } // constructor


  // степен на полиномот
  get n() {
    // наместо да го држиме степенот n на полиномот во променлива,
    // динамички го пресметуваме како должината на
    // низата од коефициентите минус еден!
    return this.a.length - 1 ;
  } // get n()


  srediNuli() {

    // ги бришеме нулите и непостоечките коефициенти оддесно
    // иако непостоечки не треба да има во внимателно напишана програма
    while (this.a.length>0 &&
          (!this.a[this.a.length-1] || this.a[this.a.length-1]==0))
      this.a.pop();

    // ако се испразнат сите коефициенти, враќаме 0 на почеток
    if (this.a.length==0)
      this.a.push(0);

    // приватнa променливa како бројач
    var i5;
    // евентуалните непостоечки коефициенти ги заменуваме со нули
    for (i5 = 0; i5 < this.a.length; i5++)
      if (!this.a[i5])
        this.a[i5]=0;

  } // srediNuli


  // собирање со втор полином p2
  soberi (p2) {

    // nMax е највисокиот степен на двата полиноми
    var nMax = this.n;
    if (p2.n > nMax)
      nMax = p2.n;

    var zbirA=[], i5;

    for (i5 = 0; i5 <= nMax; i5++) {
      zbirA[i5] = this.a[i5];
      // корекција кога не постои коефициентот a_i5
      if (!zbirA[i5])
        zbirA[i5] = 0;
      if (p2.a[i5])
        zbirA[i5] += p2.a[i5];
    }

    // за да биде збирот полином како новосоздаден објект
    return new Polinom(zbirA);

  }


  // одземање на намалител полином p2
  odzemi (p2) {

    // за да направиме полином со спротивни коефициенти
    // на намалителот p2, првин правиме копија на коефициентите
    var sprotivenP2 = new Polinom(p2.a);

    // потоа со for циклус ги менуваме знаците на сите коефициенти
    var i5;
    for (i5 = 0; i5 <= sprotivenP2.n; i5++)
      sprotivenP2.a[i5] = -sprotivenP2.a[i5];

    // на крајот одземањето полиноми се сведува на
    // собирање со спротивен полином
    return this.soberi(sprotivenP2);

  }


  // собирање со втор полином p2
  pomnozhi (p2) {

    // степенот на производот е збир од степените на множителите
    var nProizvod = this.n + p2.n;

    var proizvodA=[], i5, j5;

    // на почетокот ги поставуваме сите коефициенти на
    // производот да се нули, за да можеме после да додаваме
    for (i5 = 0; i5 <= nProizvod; i5++)
      proizvodA[i5]=0;

    for (i5 = 0; i5 <= this.n; i5++)
      for (j5 = 0; j5 <= p2.n; j5++)
        proizvodA[i5+j5] += this.a[i5] * p2.a[j5]
    // го додаваме производот на двата коефициенти на претходниот збир
    // така во коефициентот 3 на производот ќе учествуваат
    // p_0*q_3 + p_1*q_2 + p_2*q_1 + p_3*q_0


    // производот е полином кој го враќаме како новосоздаден објект
    return new Polinom(proizvodA);

  }


  // степенување на природен број m
  stepenuvaj (m) {

    // резултатот на почетокот е 1, за да го опфатиме и случајот
    // со степенување со 0, кога било што на нулти степен е 1
    var rezultat = new Polinom([1]);

    var i5;
    // степенувањето се сведува на повторено множење m-пати
    for (i5 = 1; i5 <= m; i5++)
      rezultat = this.pomnozhi(rezultat);


    // степенот е полином кој го враќаме како веќе создаден објект
    return rezultat;

  }


  // расчленување (парсирање) на полиномот според влезен стринг s
  static raschleni (s) {

    var iznos = [], tipIznos = [], vrednostIznos = [], iZ = 0;
    var operator = [];

    var b, z;
    for (b = 0; b < s.length; b++) {
      iznos[iZ] = "";
      z = s[b];

      if (z == "(") {
        // имаме израз во загради, кој треба да го
        // одделиме од остатокот од стрингот
        tipIznos[iZ] = "zagrada";

        // броиме колку загради има
        var brZagradi=1;
        b++;

        while (brZagradi>0) {
          iznos[iZ] += s[b];
          b++;

          // го зголемуваме или намалуваме бројот на загради во длабочина,
          // во зависност дали се отвораат или затвораат
          if (s[b]=="(")
            brZagradi++;
          if (s[b]==")")
            brZagradi--;
        } // одделување на израз во загради
      }
      else if (vidZnak(z)=="promenliva") {
        tipIznos[iZ] = "promenliva";
        iznos[iZ] += z;
      }
      else if (vidZnak(z)=="broj") {
        tipIznos[iZ] = "broj";
        iznos[iZ] += z;
        // го земаме бројот со сите негови знаци
        while (vidZnak(z = s[b+1]) == "broj") {
          iznos[iZ] += z;
          b++;
        }
      }
      else {
        console.log("Greshka: vo izrazot ima nedozvolen znak " + z);
      };

      // ако е исцрпен стрингот до крај, да излеземе од for b циклусот
      if (b+1 == s.length)
        break
      else {
        // ако не е исцрпен стрингот до крај, земаме нов карактер
        // и очекуваме тој да е оператор
        b++;
        z = s[b];

        if (vidZnak(z) == "operator") {
          operator[iZ] = z;
        }

      };

      // стрингот не е исцрпен до крај, очекуваме нов износ
      iZ++;

    }; // for b


    console.log("\nBroj na iznosi: ", iZ);
    console.log(iznos);
    console.log(tipIznos);
    console.log(operator);

    // овде завршува расчленувањето на изразот, па одиме на пресметување
    // на изразот, при што броевите и променливата ги заменуваме
    // со полиноми, а изразите во заграда ги пресметуваме рекурзивно

    var i5;
    for (i5 = 0; i5 <= iZ; i5++) {
      if (tipIznos[i5] == "broj")
        vrednostIznos[i5] = new Polinom(parseFloat(iznos[i5]))
      else if (tipIznos[i5] == "promenliva")
        // тогаш знаеме дека променливата е всушност x
        vrednostIznos[i5] = new Polinom(iznos[i5])
      else if (tipIznos[i5] == "zagrada")
        // ако е израз во загради, РЕКУРЕНТНО го повикуваме
        // методот Polinom.raschleni за да добиеме вредност
        vrednostIznos[i5] = this.raschleni(iznos[i5])
      else
        console.log("Greshka: neprepoznaen tip na izrazot.");

    }; // for i5
    // после овој циклус, сите вредности на износите
    // веќе ни се претворени во облик на објект Polinom
    // console.log(vrednostIznos);

    // Ако има само еден износ, нема потреба за операции,
    // тој е всушност резултатот на методот raschleni
    if (iZ == 0)
      return vrednostIznos[0];


    // Ако се повеќе вредности, останува уште да ги извршиме
    // операциите меѓу вредностите, кои се полиноми, но по приоритет
    while (operator.length > 0) {

      for (i5=0; i5<operator.length; i5++)
        // најприоритетна е операцијата на степенување
        if (operator[i5] == "^") {
          //console.log("Pred operacija ^ ", vrednostIznos);
          vrednostIznos[i5] = vrednostIznos[i5].stepenuvaj(vrednostIznos[i5+1].a[0]);
          //console.log("Stepenuvanje: ", vrednostIznos[i5].a);
          otstraniIndex(i5);
          //console.log("Posle operacija ^ ", vrednostIznos);

          // за спедната проверка на операција да започне на истото место
          i5--;
        };

        for (i5=0; i5<operator.length; i5++) {
          // втори по приоритет се множењети и делењето
          if (operator[i5] == "*") {
            vrednostIznos[i5] = vrednostIznos[i5].pomnozhi(vrednostIznos[i5+1]);
            otstraniIndex(i5);
            i5--;
          };
        };

        for (i5=0; i5<operator.length; i5++) {
          // на крајот со најмал приоритет се собирањето и одземањето
          if (operator[i5] == "+") {
            vrednostIznos[i5] = vrednostIznos[i5].soberi(vrednostIznos[i5+1]);
            otstraniIndex(i5);
            i5--;
          };

          if (operator[i5] == "-") {
            vrednostIznos[i5] = vrednostIznos[i5].odzemi(vrednostIznos[i5+1]);
            otstraniIndex(i5);
            i5--;
          };
        };

      //break;
    };

    // помошна функција за скратување на низата операции
    // и пресметани износи
    function otstraniIndex(i6) {
      operator.splice(i6, 1);
      vrednostIznos.splice(i6+1, 1);
      // кај вредностите, резултатот е на местото на левиот
      // оператор, додека десниот го бришеме
    };

    // после извршените операции по правилниот редослед на
    // приоритети, конечниот резултат од изразот s се наоѓа
    // како полином во vrednostIznos[0]
    return vrednostIznos[0];

//      rezultat = this.pomnozhi(rezultat);


  } // raschleni, статичка + рекурзивен метод




}; // class Polinom


// помошна функција за одредување на видот на знакот
// кој е дел од влезниот стринг
function vidZnak(z) {
  if (z=="x")
    return "promenliva";
  if ((z>="0" && z<="9") || z==",")
    return "broj";
  if (z=="+" || z=="-" || z=="*" || z=="/" || z=="^")
    return "operator";
  if (z=="(" || z==")")
    return "zagrada";
  if (z==" ")
    return "prazno";
  if (!z)
    return "nishto";
  return "nedozvoleno"
};

/*
var p = new Polinom([1, 2, 0, 2]);
var q = new Polinom([0, , 0, 2, ]);

console.log(p.soberi(q))
console.log(p.odzemi(q))
*/

/*
var p = new Polinom([1, -2]);

console.log(p.pomnozhi(p).pomnozhi(p))
console.log(p.stepenuvaj(5))
*/

console.log(new Polinom("x"));

Polinom.raschleni("123+(2)-3");

console.log(Polinom.raschleni("2^3^2"));
console.log(Polinom.raschleni("(1+2*x)^2"));
