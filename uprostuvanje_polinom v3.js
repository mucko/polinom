// Упростување на алгебарски изрази до полиномен облик

//
'use strict';


class Polinom {

  // конструктор за нов полином како објект
  constructor(dadenP) {
    // како вредност на dadenP може да биде: низа од коефициенти,
    // константа (број) или променлива x

    if (Array.isArray(dadenP)) {

      // правиме копија на влезната низа од коефициенти
      // кои се од облик a_0, a_1, ... , a_n
      this.a = dadenP.slice();

      //ги бришеме нулите и непостоечките коефициенти оддесно
      this.srediNuli();

      console.log(this.a);

    }
    else {

    }
  } // constructor


  // степен на полиномот
  get n() {
    // наместо да го држиме степенот n на полиномот во променлива,
    // динамички го пресметуваме како должината на
    // низата од коефициентите минус еден!
    return this.a.length - 1 ;
  } // get n()


  srediNuli() {

    // ги бришеме нулите и непостоечките коефициенти оддесно
    // иако непостоечки не треба да има во внимателно напишана програма
    while (this.a.length>0 &&
          (!this.a[this.a.length-1] || this.a[this.a.length-1]==0))
      this.a.pop();

    // ако се испразнат сите коефициенти, враќаме 0 на почеток
    if (this.a.length==0)
      this.a.push(0);

    // приватнa променливa како бројач
    var i5;
    // евентуалните непостоечки коефициенти ги заменуваме со нули
    for (i5 = 0; i5 < this.a.length; i5++)
      if (!this.a[i5])
        this.a[i5]=0;

  } // srediNuli


  // собирање со втор полином p2
  soberi (p2) {

    // nMax е највисокиот степен на двата полиноми
    var nMax = this.n;
    if (p2.n > nMax)
      nMax = p2.n;

    var zbirA=[], i5;

    for (i5 = 0; i5 <= nMax; i5++) {
      zbirA[i5] = this.a[i5];
      // корекција кога не постои коефициентот a_i5
      if (!zbirA[i5])
        zbirA[i5] = 0;
      if (p2.a[i5])
        zbirA[i5] += p2.a[i5];
    }

    // за да биде збирот полином како новосоздаден објект
    return new Polinom(zbirA);

  }


  // одземање на намалител полином p2
  odzemi (p2) {

    // за да направиме полином со спротивни коефициенти
    // на намалителот p2, првин правиме копија на коефициентите
    var sprotivenP2 = new Polinom(p2.a);

    // потоа со for циклус ги менуваме знаците на сите коефициенти
    var i5;
    for (i5 = 0; i5 <= sprotivenP2.n; i5++)
      sprotivenP2.a[i5] = -sprotivenP2.a[i5];

    // на крајот одземањето полиноми се сведува на
    // собирање со спротивен полином
    return this.soberi(sprotivenP2);

  }


  // собирање со втор полином p2
  pomnozhi (p2) {

    // степенот на производот е збир од степените на множителите
    var nProizvod = this.n + p2.n;

    var proizvodA=[], i5, j5;

    // на почетокот ги поставуваме сите коефициенти на
    // производот да се нули, за да можеме после да додаваме
    for (i5 = 0; i5 <= nProizvod; i5++)
      proizvodA[i5]=0;

    for (i5 = 0; i5 <= this.n; i5++)
      for (j5 = 0; j5 <= p2.n; j5++)
        proizvodA[i5+j5] += this.a[i5] * p2.a[j5]
    // го додаваме производот на двата коефициенти на претходниот збир
    // така во коефициентот 3 на производот ќе учествуваат
    // p_0*q_3 + p_1*q_2 + p_2*q_1 + p_3*q_0


    // производот е полином кој го враќаме како новосоздаден објект
    return new Polinom(proizvodA);

  }


  // степенување на природен број m
  stepenuvaj (m) {

    // резултатот на почетокот е 1, за да го опфатиме и случајот
    // со степенување со 0, кога било што на нулти степен е 1
    var rezultat = new Polinom([1]);

    var i5;
    // степенувањето се сведува на повторено множење m-пати
    for (i5 = 1; i5 <= m; i5++)
      rezultat = this.pomnozhi(rezultat);


    // степенот е полином кој го враќаме како веќе создаден објект
    return rezultat;

  }


  // расчленување (парсирање) на полиномот според влезен стринг s
  static raschleni (s) {

    var iznos = [], tipIznos = [], iZ = 0;
    var operator = [];

    var b, z;
    for (b = 0; b < s.length; b++) {
      iznos[iZ] = "";
      z = s[b];

      if (z == "(") {
        // имаме израз во загради, кој треба да го
        // одделиме од остатокот од стрингот
        tipIznos[iZ] = "zagrada";

        // броиме колку загради има
        var brZagradi=1;
        b++;

        while (brZagradi>0) {
          iznos[iZ] += s[b];
          b++;

          // го зголемуваме или намалуваме бројот на загради во длабочина,
          // во зависност дали се отвораат или затвораат
          if (s[b]=="(")
            brZagradi++;
          if (s[b]==")")
            brZagradi--;
        } // одделување на израз во загради

      }
      else {
        tipIznos[iZ] = "broj";
        iznos[iZ] += z;
        // го земаме бројот со сите негови знаци
        while (vidZnak(z = s[b+1]) == "broj") {
          iznos[iZ] += z;
          b++;
        }
      }


      // ако е исцрпен стрингот до крај, да излеземе од for b циклусот
      if (b+1 == s.length)
        break
      else {
        // ако не е исцрпен стрингот до крај, земаме нов карактер
        // и очекуваме тој да е оператор
        b++;
        z = s[b];

        if (vidZnak(z) == "operator") {
          operator[iZ] = z;
        }

      };

      // стрингот не е исцрпен до крај, очекуваме нов износ
      iZ++;

    }; // for b

    // овде завршува расчленувањето на изразот, па одиме на пресметување
    // на изразот, при што броевите и променливата ги заменуваме
    // со полиноми, а изразите во заграда ги пресметуваме рекурзивно

    console.log(iznos);
    console.log(tipIznos);
    console.log(operator);


  } // raschleni




}; // class Polinom


// помошна функција за одредување на видот на знакот
// кој е дел од влезниот стринг
function vidZnak(z) {
  if (z=="x")
    return "promenliva";
  if ((z>="0" && z<="9") || z==",")
    return "broj";
  if (z=="+" || z=="-" || z=="*")
    return "operator";
  if (z=="(" || z==")")
    return "zagrada";
  if (z==" ")
    return "prazno";
  if (!z)
    return "nishto";
  return "nedozvoleno"
};

/*
var p = new Polinom([1, 2, 0, 2]);
var q = new Polinom([0, , 0, 2, ]);

console.log(p.soberi(q))
console.log(p.odzemi(q))
*/

/*
var p = new Polinom([1, -2]);

console.log(p.pomnozhi(p).pomnozhi(p))
console.log(p.stepenuvaj(5))
*/

Polinom.raschleni("123+(2)-3")
