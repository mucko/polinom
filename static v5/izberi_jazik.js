// Избор на јазик на корисничкиот интерфејс, при што главни јазици се македонски и англиски,
// а има превод и на албански, турски и српски кои се користат во образованието во Република Македонија

//
'use strict';

// Проверка на поддржаните јазици кај прелистувачот (browser)
// console.log("Jazici kaj prelistuvachot:");
// console.log(navigator.languages);

var izbranJazik, listaJazici=["en", "mk", "sr", "sq", "tr"];

// izbranJazik = "";
izbranJazik = getCookie("izbranJazik");

/*
console.log(izbranJazik);
console.log("aa" || "bb");
console.log("aa" || undefined);
console.log(undefined || "bb");
*/

var i3, j3, mozhenJazik;
// ако не е избран јазик, се зема најповолниот од прелистувачот
if (!izbranJazik) {
  for (i3=0; i3<navigator.languages.length; i3++) {

    mozhenJazik = navigator.languages[i3].slice(0, 2);
    // console.log("Mozhen jazik: ", mozhenJazik);
    j3 = listaJazici.indexOf(mozhenJazik);
    if (j3>0) {
      // избран е јазикот!
      izbranJazik = mozhenJazik;
      break; // за излез од циклусот for i3
    }

  } // for i3

  // ако и покрај сè не е избран јазик, се подразбира англиски
  if (!izbranJazik)
    izbranJazik = "en";
};

// избраниот јазик го запишуваме во колаче, кое трае 400 дена = година и кусур
setCookie("izbranJazik", izbranJazik, 400);
console.log("Izbran jazik e: " + izbranJazik);


/*
var jazik=[];
jazik["en"]=[];
jazik.en[2]="Two";
console.log(jazik);
console.log(jazik["en"]["2"]);
*/

var jazik = [];
jazik["en"] = [];
jazik["mk"] = [];
jazik["sr"] = [];
jazik["sq"] = [];
jazik["tr"] = [];

jazik.ID =
jazik["en"].ID = "ENGLISH";
jazik["mk"].ID = "МАКЕДОНСКИ";
jazik["sr"].ID = "СРПСКИ";
jazik["sq"].ID = "SHQIP";
jazik["tr"].ID = "TÜRKÇE";



// функцијата за додавање на динамички елементи се извршува ПОСЛЕ целосното
// вчитување на body и извршувањето на претходните обични скрипти што
// не се дел од функција. Со оваа функција динамички го креираме менито
// за избор на јазик, при што најгоре е постоечкиот јазик
function dodajDinamichkiElementi() {

  // return;

  // динамичко креирање на менито за избор на јазик
  var meni_Jazik = document.getElementById("meni_jazik");
  var i3, j3, meniHTML = "";

  var meni_listaJazici = [izbranJazik]; // прв елемент на листата на јазици во менито е избраниот
  // а понатаму јазиците си се по стандардниот редослед
  for (i3=0; i3<listaJazici.length; i3++)
    if (listaJazici[i3]!=izbranJazik)
      meni_listaJazici.push(listaJazici[i3]);

  for (i3=0; i3<meni_listaJazici.length; i3++)
    meniHTML += "<li onclick=\"smeniJazik(&quot;" + meni_listaJazici[i3] + "&quot;)\"><img src=\"ikoni/" + meni_listaJazici[i3] + ".png\"/>" + jazik[meni_listaJazici[i3]].ID + "</li>";
    // <li onclick="smeniJazik(&quot;mk&quot;)"><img src="ikoni/mk.png"/>МАКЕДОНСКИ</li>

    // meniHTML += "<li><img src=\"ikoni/" + listaJazici[i3] + ".png\"/>" + jazik[listaJazici[i3]].ID + "</li>";
    // <li><img src="ikoni/mk.png"/>МАКЕДОНСКИ</li>

  // откако е составен големиот стринг за праѓачкото мени за избор на јазик,
  // одеднаш го ставаме како внатрешен HTML
  meni_Jazik.innerHTML = meniHTML;

};


function smeniJazik(novJazik) {

  console.log("Treba da se smeni jazikot vo " + novJazik);

  if (novJazik==izbranJazik) {

    // нема потреба да се менува јазикот, но ќе му се укаже на корисникот за кликот
    var j3 = document.getElementsByTagName("li")[0];
    // j3.innerHTML="***";
    // j3.style.transitionProperty = "none";
    //j3.style.backgroundColor = "#FEC"; // не може да се смени додека е hover

    //j3.transition: all .5s ease;
    //j3.style.transition = "all 1s";
    //j3.style.backgroundColor = "#FFF";
    // j3.style.color = "#0EC"; // не може да се смени додека е hover
    //j3.style.color = "#000"; // не може да се смени додека е hover

    j3.classList.add("zlatenSjaj");
    setTimeout(function(){
      j3.classList.remove("zlatenSjaj");;
    }, 700);


  }
  else {

    // новиот јазик го запишуваме во колаче
    setCookie("izbranJazik", novJazik, 400);
    // за да фатат промените на јазикот, го превчитуваме одново целиот HTML документ
    location.reload();

  };
};

// готова функција за запишување на колаче
function setCookie(c_name, value, exdays) {
    var exdate = new Date();
    exdate.setDate(exdate.getDate() + exdays);
    var c_value = escape(value) + ((exdays == null) ? "" : "; expires=" + exdate.toUTCString());
    document.cookie = c_name + "=" + c_value;
};

// готова функција за читање на колаче
function getCookie(c_name) {
    var i, x, y, ARRcookies = document.cookie.split(";");
    for (i = 0; i < ARRcookies.length; i++) {
        x = ARRcookies[i].substr(0, ARRcookies[i].indexOf("="));
        y = ARRcookies[i].substr(ARRcookies[i].indexOf("=") + 1);
        x = x.replace(/^\s+|\s+$/g, "");
        if (x == c_name) {
            return unescape(y);
        };
    };

    // ако нема колече, да врати резултат дека не е дефинирано
    return undefined;
};
