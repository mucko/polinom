// Избор на јазик на корисничкиот интерфејс, при што главни јазици се македонски и англиски,
// а има превод и на албански, турски и српски кои се користат во образованието во Република Македонија

//
'use strict';

// Проверка на поддржаните јазици кај прелистувачот (browser)
// console.log("Jazici kaj prelistuvachot:");
// console.log(navigator.languages);

var izbranJazik, listaJazici=["en", "mk", "sr", "sq", "tr"];

izbranJazik = ""; // getCookie("izbranJazik");
/*
console.log(izbranJazik);
console.log("aa" || "bb");
console.log("aa" || undefined);
console.log(undefined || "bb");
*/

var i3, j3, mozhenJazik;
// ако не е избран јазик, се зема најповолниот од прелистувачот
if (!izbranJazik) {
  for (i3=0; i3<navigator.languages.length; i3++) {

    mozhenJazik = navigator.languages[i3].slice(0, 2);
    // console.log("Mozhen jazik: ", mozhenJazik);
    j3 = listaJazici.indexOf(mozhenJazik);
    if (j3>0) {
      // избран е јазикот!
      izbranJazik = mozhenJazik;
      break; // за излез од циклусот for i3
    }

  } // for i3

  // ако и покрај сè не е избран јазик, се подразбира англиски
  if (!izbranJazik)
    izbranJazik = "en";
};

// избраниот јазик го запишуваме во колаче, кое трае 400 дена = година и кусур
setCookie("izbranJazik", izbranJazik, 400);
console.log("Izbran jazik e: " + izbranJazik);


/*
var jazik=[];
jazik["en"]=[];
jazik.en[2]="Two";
console.log(jazik);
console.log(jazik["en"]["2"]);
*/



// готова функција за запишување на колаче
function setCookie(c_name, value, exdays) {
    var exdate = new Date();
    exdate.setDate(exdate.getDate() + exdays);
    var c_value = escape(value) + ((exdays == null) ? "" : "; expires=" + exdate.toUTCString());
    document.cookie = c_name + "=" + c_value;
};

// готова функција за читање на колаче
function getCookie(c_name) {
    var i, x, y, ARRcookies = document.cookie.split(";");
    for (i = 0; i < ARRcookies.length; i++) {
        x = ARRcookies[i].substr(0, ARRcookies[i].indexOf("="));
        y = ARRcookies[i].substr(ARRcookies[i].indexOf("=") + 1);
        x = x.replace(/^\s+|\s+$/g, "");
        if (x == c_name) {
            return unescape(y);
        };
    };

    // ако нема колече, да врати резултат дека не е дефинирано
    return undefined;
};
