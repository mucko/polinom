// Цртање на графикот на полиномот на платното (canvas),
// на кој претходно има координатна мрежа

//
'use strict';

class Grafik {

  // конструктор што се повикува при првата употреба на графикот
  constructor(elementGrafikId) {

    var grafik = document.getElementById(elementGrafikId);
    this.G = grafik.getContext("2d");

    this.G.lineWidth = 0.5;

    this.G.moveTo(-10,0);
    this.G.lineTo(200,100);
    this.G.lineTo(250,100);
    this.G.stroke();

    //var ctx = c.getContext("2d");
    this.G.beginPath();
    this.G.arc(95,50,70,0,2*Math.PI);
    this.G.stroke();

  } // constructor

} // class Grafik


// На web-страната користиме само еден објект grafik од класата Grafik,
// па овде го иницијализираме
// var grafik = new Grafik("grafik_canvas");
