var express = require('express');
var app = express();
var path = require('path');
var http = require('http').Server(app);
var io = require('socket.io')(http);


// Датотеките што се сервираат без да се менуваат, се во фолдерот static
// app.use(express.static('public'));
app.use(express.static(path.join(__dirname, 'static')));

app.get('/', function(req, res) {
  res.sendFile('index.html');
  // можеме вака кратко бидејќи е во статичкиот фолдер, инаку полниот израз е
  // res.sendFile(path.join(__dirname, 'static/index.html'));
});


var users = [];
io.on('connection', function(socket) {
   console.log('Uchenikot go otvori portalot za polinomi.');

   /*
   socket.on('setUsername', function(data) {
      console.log(data);

      if(users.indexOf(data) > -1) {
         socket.emit('userExists', data + ' username is taken! Try some other username.');
      } else {
         users.push(data);
         socket.emit('userSet', {username: data});
      }
   });
   */

   // Евентот 'ispratiPolinom' се јавува кога од прелистувачот се испраќа нов полином
   socket.on('ispratiPolinom', function(polinomZadaden) {
      // Испратениот полином треба да се сподели, од серверот кон
      // отворените прелистувачи на сите ученици освен наставникот (тој е socket)
      socket.broadcast.emit('spodeliPolinom', polinomZadaden);

      // ако се употреби io.sockets пораката се испраќа и до САМИОТ СЕБЕСИ
      // io.sockets.emit('spodeliPolinom', polinomZadaden);
   })
});

http.listen(3000, function() {
   console.log('Serverot slusha na http:\\localhost:3000');
});
