// Упростување на алгебарски изрази до полиномен облик

//
'use strict';


class Polinom {

  // конструктор за нов полином како објект
  constructor(dadenP) {
    // како вредност на dadenP може да биде: низа од коефициенти,
    // константа (број) или променлива x

    if (Array.isArray(dadenP)) {

      // правиме копија на влезната низа од коефициенти
      // кои се од облик a_0, a_1, ... , a_n
      this.a = dadenP.slice();

      //ги бришеме нулите и непостоечките коефициенти оддесно
      this.srediNuli();

      console.log(this.a);

    }
    else {

    }
  } // constructor


  // степен на полиномот
  get n() {
    // наместо да го држиме степенот n на полиномот во променлива,
    // динамички го пресметуваме како должината на
    // низата од коефициентите!
    return this.a.length;
  } // get n()


  srediNuli() {

    // ги бришеме нулите и непостоечките коефициенти оддесно
    // иако непостоечки не треба да има во внимателно напишана програма
    while (this.a.length>0 &&
          (!this.a[this.a.length-1] || this.a[this.a.length-1]==0))
      this.a.pop();

    // ако се испразнат сите коефициенти, враќаме 0 на почеток
    if (this.a.length==0)
      this.a.push(0);

    // приватнa променливa како бројач
    var i5;
    // евентуалните непостоечки коефициенти ги заменуваме со нули
    for (var i5 = 0; i5 < this.a.length; i5++)
      if (!this.a[i5])
        this.a[i5]=0;

  } // srediNuli


  // собирање со втор полином p2
  soberi (p2) {

    // nMax е највисокиот степен на двата полиноми
    var nMax = this.n;
    if (p2.n > nMax)
      nMax = p2.n;

    var zbirA=[], i5;

    for (i5 = 0; i5 < nMax; i5++) {
      zbirA[i5] = this.a[i5];
      // корекција кога не постои коефициентот a_i5
      if (!zbirA[i5])
        zbirA[i5] = 0;
      if (p2.a[i5])
        zbirA[i5] += p2.a[i5];
    }

    // за да биде збирот полином како новосоздаден објект
    return new Polinom(zbirA);

  }

}

var p = new Polinom([1, 2, 0, 2]);
var q = new Polinom([0, , 0, 2, ]);

console.log(p.soberi(q))
